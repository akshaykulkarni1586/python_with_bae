class AmountError(Exception):
    """ AmountError Class custom exception"""

    def __init__(self, account, msg):
        self.account = account
        self.msg = msg

    def __str__(self):
        return 'AmountError (' + str(self.msg) + ') ' + str(self.account)

class BalanceError(Exception):
    """ BalanceError Class custom exception"""

    def __init__(self, account):
        self.account = account

    def __str__(self):
        return 'BalanceError ( Withdrawal would exceed your overdraft limit ) ' + str(self.account)

class Account:
    """ This class represents bank account"""

    cnt_account = 0
    
    def __init__(self, acc_num, name, open_bal, type_acc):
        Account.count_account()
        self.acc_num = acc_num
        self.name = name
        self._balance = open_bal  #for a property its mandatory to
                                  # have '_' before name of attr
        self.type = type_acc

    def __str__(self):
        return 'Account[' + str(self.acc_num) + '] - ' + self.name + ', ' \
               + self.type + ' account = ' + str(self.balance)

    def deposit(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot deposit negative amounts')

        self._balance += amount
                    
    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self._balance - amount) > 0:
            self._balance -= amount
        else:
            print('Low balance')
        

    @property    
    def balance(self):
        """ This is balace property"""
        return self._balance

    @classmethod
    def count_account(cls):
        cls.cnt_account += 1
        print('A new account is created. Total accounts: ', cls.cnt_account)

    @staticmethod
    def stand_Alone():
        print('Doesnt need any parameter, neither cls nor self')


class CurrentAccount(Account):
    def __init__(self, acc_num, name, open_bal, overdraft_limit):
        super().__init__(acc_num, name, open_bal, 'Current')
        self.overdraft_limit = overdraft_limit

    def __str__(self):
        return super().__str__() + ' overdraft limit is: '+ \
               str(self.overdraft_limit)

    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self.balance - amount) < self.overdraft_limit:
            raise BalanceError(self)
            #print('Withdrawal would exceed your overdraft limit')
        else :
            super().withdraw(amount)


class DepositAccount(Account):
    def __init__(self, acc_num, name, open_bal, interest_rate):
        super().__init__(acc_num, name, open_bal, 'Deposit')
        self.interest_rate = interest_rate

    def __str__(self):
        return super().__str__() + ' interest rate is: '+ \
               str(self.interest_rate)


class InvestmentAccount(Account):
    def __init__(self, acc_num, name, open_bal, investment_type):
        super().__init__(acc_num, name, open_bal, 'Investment')
        self.investment_type = investment_type

    def __str__(self):
        return super().__str__() + ' investment type is: '+ \
               self.investment_type
def test():
    acc1 = Account('123', 'John', 10.05, 'current')
    acc2 = Account('345', 'John', 23.55, 'savings')
    acc3 = Account('567', 'Phoebe', 12.45, 'investment')

    print(acc1)
    print(acc2)
    print(acc3)

    acc1.deposit(23.45)
    acc1.withdraw(12.33)
    print('balance:', acc1.balance)
    print('Number of Account instances created:', Account.cnt_account)

def test2():
    # CurrentAccount(account_number, account_holder,
    # opening_balance, overdraft_limit)

    acc1 = CurrentAccount('123', 'John', 10.05, 100.0)
    print(acc1)
    
    # DepositAccount(account_number, account_holder,
    # opening_balance, interest_rate)

    acc2 = DepositAccount('345', 'John', 23.55, 0.5)
    print(acc2)
    
    # InvestmentAccount(account_number, account_holder,
    # opening_balance, investment_type)

    acc3 = InvestmentAccount('567', 'Phoebe', 12.45, 'high risk')
    print(acc3)
    
    acc1.deposit(123.45)
    try:
        acc1.withdraw(12.33)
        print('balance:', acc1.balance)
    except Exception as e:
        print(e)

    try:
        acc1.withdraw(300.00)
        print('balance:', acc1.balance)
    except Exception as e:
        print(e)

        
def test3():

    acc1 = CurrentAccount('123', 'John', 21.05, 100.0)
    try:
        acc1.deposit(-1)
    except AmountError as e:
        print(e)

    try:
        acc1.withdraw(-1)
    except AmountError as e:
        print(e)

    try:
        print('balance:', acc1.balance)
        acc1.withdraw(300.00)
        print('balance:', acc1.balance)
    except BalanceError as e:
        print('Handling Exception')
        print(e)
    
##main##
test()
print('-'*25)
test2()
print('-'*25)
test3()
