## This is chaining of exception. using keyword 'raise' and 'from' ##

class DivideByYWhenZeroException(Exception):
   """ A customed exception"""
   def __str__(self):
       return 'DivideByYWhenZeroException exception'

def divide(x, y):
    try:
        result = x /y
        return result
    except Exception as e:
        print(e)
        raise DivideByYWhenZeroException from e      #this is imp line
        #raise
    

def main():
    print(divide(1,2))

    try:
        x = divide(3, 0)
        print(x)
    except Exception as e:
        print(e)

main()
    
    
