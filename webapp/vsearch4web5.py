from flask import Flask, render_template, request, escape
import vsearch
from DBcm import UseDatabase

app = Flask(__name__)

config='vsearchlogDB.db'
def log_request(req: 'flask_request', res: str) -> None:
      
    with UseDatabase(config) as cursor:
        _SQL="""Insert into log
                (phrase, letters, ip, browser_string, results)
                values
                (?, ?, ?, ?, ?)"""
        cursor.execute(_SQL, (req.form['phrase'],
                                  req.form['letters'],
                                  req.remote_addr,
                                  req.user_agent.browser,
                                  res, ))   
    
@app.route('/search4', methods=['POST'])
def do_search() -> 'html':
    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your results'
    result = str(vsearch.search4letters(phrase, letters))
    log_request(request, result)
    return render_template('results.html', 
                           the_title = title,
                           the_phrase = phrase,
                           the_letters = letters,
                           the_result = result)

@app.route('/')    
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', 
        the_title='Welcome to search4letters on the web')

@app.route('/viewlog')
def view_the_log() -> 'html':
    with UseDatabase(config) as cursor:
        _SQL="""SELECT phrase, letters, ip, browser_string, results from log"""
        cursor.execute(_SQL)
        contents=cursor.fetchall()
        
        titles=['Phrase', 'Letters', 'Remote Addr', 'User agent', 'Results']
       
        return render_template('viewlog.html', 
                               the_title='View log',
                               the_row_titles=titles,
                               the_data=contents)
if __name__ == '__main__':
    app.run(debug=True)
