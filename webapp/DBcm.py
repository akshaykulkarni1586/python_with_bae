import sqlite3

class UseDatabase:

    def __init__(self, config:str) ->None:
        self.configuration=config

    def __enter__(self) -> 'cursor':
        """connects to database with the given configuration
           and returns cursor"""
        self.conn=sqlite3.connect(self.configuration)
        self.cursor=self.conn.cursor()

        return self.cursor

    def __exit__(self, exc_type, exc_value, exc_trace) ->None:
        """commits all cache and closes all connections"""
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
