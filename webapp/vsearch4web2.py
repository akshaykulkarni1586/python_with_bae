from flask import Flask, render_template, request, escape
import vsearch

app = Flask(__name__)

def log_request(req: 'flask_request', res: str) -> None:
    with open('vsearch2.log', 'a') as log:
        print(req.form, req.remote_addr, req.user_agent, res, file=log, sep='|')
        
@app.route('/search4', methods=['POST'])
def do_search() -> 'html':
    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your results'
    result = str(vsearch.search4letters(phrase, letters))
    log_request(request, result)
    return render_template('results.html', 
                           the_title = title,
                           the_phrase = phrase,
                           the_letters = letters,
                           the_result = result)

@app.route('/')    
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', \
        the_title='Welcome to search4letters on the web')

@app.route('/viewlog')
def view_the_log() -> str:
    with open('vsearch2.log') as log:
        contents = log.read()
    return escape(contents)

if __name__ == '__main__':
    app.run(debug=True)
