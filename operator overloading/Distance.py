###Operator Overloading ##

class Distance:

    def __init__(self, val):
        self.val = val

    def __str__(self):
        return 'Distance[' + str(self.val) + ']'
    
    def __add__(self, other):
        return Distance(self.val + other.val)

    def __sub__(self, other):
        return Distance(self.val - other.val)

    def __mul__(self, other):
        if isinstance(other, Distance):
            val = self.val * other.val
        else :
            val = self.val * other
        return Distance(val)

    def __truediv__(self, other):
        if isinstance(other, Distance):
            val = self.val / other.val
        else :
            val = self.val / other
        return Distance(val)

    def __floordiv__(self, other):
        if isinstance(other, Distance):
            val = self.val // other.val
        else :
            val = self.val // other
        return Distance(val)

def test():
    d1 = Distance(6)
    d2 = Distance(3)
    print( d1 + d2)
    print (d1 - d2)
    print (d1 / 2)
    print(d2 // 2)
    print(d2 * 2)

test()
