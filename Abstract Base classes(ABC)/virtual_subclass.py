from abc import ABCMeta, abstractmethod

class Person(metaclass=ABCMeta):

    def __init__(self, name, age):
        self.name = name
        self.age = age

    @abstractmethod
    def birthday(self):
        #print('Happy Birthday')
        pass


class Employee():

    def __init__(self, name, age, id):
        self.name = name
        self.age = age
        self.id = id

    def __str__(self):
        return self.name + ' age: ' + str(self.age) + ' id: ' + str(self.id)

    def birthday(self):
        print('Its your birthday')


Person.register(Employee)    ### registering a Employee class as a subclass of Person

print(issubclass(Employee, Person))
e1 = Employee('john', 34, 4545)
e1.birthday()
print(e1)
print(isinstance(e1, Person))