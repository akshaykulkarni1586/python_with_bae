from abc import ABCMeta, abstractmethod

""" This is account module """
print('Loading accounts module')


class AmountError(Exception):
    """ AmountError Class custom exception"""

    def __init__(self, account, msg):
        self.account = account
        self.msg = msg

    def __str__(self):
        return 'AmountError (' + str(self.msg) + ') ' + str(self.account)

class BalanceError(Exception):
    """ BalanceError Class custom exception"""

    def __init__(self, account):
        self.account = account

    def __str__(self):
        return 'BalanceError ( Withdrawal would exceed your overdraft limit ) ' + str(self.account)

class Account(metaclass=ABCMeta):
    """ This class represents bank account"""

    cnt_account = 0
    
    def __init__(self, acc_num, name, open_bal, type_acc):
        Account.count_account()
        self.acc_num = acc_num
        self.name = name
        self._balance = open_bal  #for a property its mandatory to
                                  # have '_' before name of attr
        self.type = type_acc

    def __str__(self):
        return 'Account[' + str(self.acc_num) + '] - ' + self.name + ', ' \
               + self.type + ' account = ' + str(self.balance)

    def deposit(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot deposit negative amounts')

        self._balance += amount
                    
    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self._balance - amount) > 0:
            self._balance -= amount
        else:
            print('Low balance')
        

    @property    
    def balance(self):
        """ This is balace property"""
        return self._balance

    @classmethod
    def count_account(cls):
        cls.cnt_account += 1
        print('A new account is created. Total accounts: ', cls.cnt_account)

    @staticmethod
    def stand_Alone():
        print('Doesnt need any parameter, neither cls nor self')
