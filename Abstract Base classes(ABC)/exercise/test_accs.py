import sub_classes_account as accounts

print('In ouside one directory importing fintech.accounts')

acc1 = accounts.CurrentAccount('123', 'John', 10.05, 100.0)
acc2 = accounts.DepositAccount('345', 'John', 23.55, 0.5)
acc3 = accounts.InvestmentAccount('567', 'Phoebe', 12.45, 'high risk')

print(acc1)
print(acc2)
print(acc3)

acc1.deposit(23.45)

try:
    acc1.withdraw(12.33)
except accounts.BalanceError as e:
    print(e)


print('balance:', acc1.balance)
print('Number of Account instances created:', \
    accounts.Account.cnt_account)

try:
    print('balance:', acc1.balance)
    acc1.withdraw(300.00)
    print('balance:', acc1.balance)
except accounts.BalanceError as e:
    print('Handling Exception')
    print(e) 

#acc4 = accounts.Account('123', 'John', 100.0, 'basic')