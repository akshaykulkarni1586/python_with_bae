from abc import ABC, abstractmethod

class Shape2(ABC):

    def __init__(self):
        print('hi in abstract')
        self._id = 50

    @abstractmethod
    def display(self):
        pass   

class Triangle(Shape2):

    def __init__(self):
        super().__init__()

    def display(self):
        print('here in triangle with id ', self._id)


t1 = Triangle()
print(t1._id)
t1.display()