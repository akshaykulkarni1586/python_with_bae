from abc import ABCMeta, abstractmethod

class Shape(metaclass=ABCMeta):

    def __init__(self, id):
        print('in abstract')
        self._id = id

    @abstractmethod
    def display(self):
        pass

    @property
    @abstractmethod
    def id(self):
        pass

