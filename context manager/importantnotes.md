**so in an context manager,**

```
with <class-object> as obj-referance:
  code
  ...  
```



- Here first class instance is created then 
- that instance is passed to 
__enter__() method. 
- self in __enter__() is the object created.
- you can try by printing the self.
thats why returning `self` is very important.