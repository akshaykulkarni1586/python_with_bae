from accounts import *

class CurrentAccount(Account):
    """ subclass of account. This class has a parameter overdraft_limit. 
    this class raises BalanceError, AmountError in withdraw method"""
    
    def __init__(self, acc_num, name, open_bal, overdraft_limit):
        print('child init')
        super().__init__(acc_num, name, open_bal, 'Current')
        self.overdraft_limit = overdraft_limit

    def __str__(self):
        return super().__str__() + ' overdraft limit is: '+ \
               str(self.overdraft_limit)

    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self.balance - amount) < self.overdraft_limit:
            raise BalanceError(self)
            #print('Withdrawal would exceed your overdraft limit')
        else :
            super().withdraw(amount)


class DepositAccount(Account):
    """ subclass of account. all methods are inherited """
    
    def __init__(self, acc_num, name, open_bal, interest_rate):
        super().__init__(acc_num, name, open_bal, 'Deposit')
        self.interest_rate = interest_rate

    def __str__(self):
        return super().__str__() + ' interest rate is: '+ \
               str(self.interest_rate)


class InvestmentAccount(Account):
    """ subclass of account. all methods are inherited """
    
    def __init__(self, acc_num, name, open_bal, investment_type):
        super().__init__(acc_num, name, open_bal, 'Investment')
        self.investment_type = investment_type

    def __str__(self):
        return super().__str__() + ' investment type is: '+ \
               self.investment_type