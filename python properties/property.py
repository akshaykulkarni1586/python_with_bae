class Person:

    def __init__(self, name, age):
        self._name = name
        self._age = age
        
    def __str__(self):
        return 'Person[' + str(self._name) +'] is ' + \
                str(self._age)
    
    def set_age(self, new_age):
        
        if isinstance(new_age, int) and new_age > 0 and new_age < 120:
            self._age = new_age
            print('Age set to new value', self._age)
        else :
            print('Warning!! age value is incorrect')

    def get_age(self):
        return self._age

    age = property(fget= get_age, fset= set_age, fdel=None, doc='Age property')

    def get_name(self):
        return self._name

    name = property(fget= get_name, doc='Name property')

p1 = Person('Shreya', 18)

print(p1.age)
p1.age = 180
print(p1.age)
p1.age = -1
print(p1.age)

p1.age = 20
print(p1.age)
print(p1)

print(p1.name)
#p1.name = 'Rima'  -----------> throws error
print(p1)
