""" This is account module """
print('Loading accounts module')


class AmountError(Exception):
    """ AmountError Class custom exception"""

    def __init__(self, account, msg):
        self.account = account
        self.msg = msg

    def __str__(self):
        return 'AmountError (' + str(self.msg) + ') ' + str(self.account)

class BalanceError(Exception):
    """ BalanceError Class custom exception"""

    def __init__(self, account):
        self.account = account

    def __str__(self):
        return 'BalanceError ( Withdrawal would exceed your overdraft limit ) ' + str(self.account)

class Account:
    """ This class represents bank account"""

    cnt_account = 0
    
    def __init__(self, acc_num, name, open_bal, type_acc):
        Account.count_account()
        self.acc_num = acc_num
        self.name = name
        self._balance = open_bal  #for a property its mandatory to
                                  # have '_' before name of attr
        self.type = type_acc

    def __str__(self):
        return 'Account[' + str(self.acc_num) + '] - ' + self.name + ', ' \
               + self.type + ' account = ' + str(self.balance)

    def deposit(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot deposit negative amounts')

        self._balance += amount
                    
    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self._balance - amount) > 0:
            self._balance -= amount
        else:
            print('Low balance')
        

    @property    
    def balance(self):
        """ This is balace property"""
        return self._balance

    @classmethod
    def count_account(cls):
        cls.cnt_account += 1
        print('A new account is created. Total accounts: ', cls.cnt_account)

    @staticmethod
    def stand_Alone():
        print('Doesnt need any parameter, neither cls nor self')


class CurrentAccount(Account):
    def __init__(self, acc_num, name, open_bal, overdraft_limit):
        super().__init__(acc_num, name, open_bal, 'Current')
        self.overdraft_limit = overdraft_limit

    def __str__(self):
        return super().__str__() + ' overdraft limit is: '+ \
               str(self.overdraft_limit)

    def withdraw(self, amount):
        if amount < 0:
            raise AmountError(self, 'Cannot withdraw negative amounts')

        if (self.balance - amount) < self.overdraft_limit:
            raise BalanceError(self)
            #print('Withdrawal would exceed your overdraft limit')
        else :
            super().withdraw(amount)


class DepositAccount(Account):
    def __init__(self, acc_num, name, open_bal, interest_rate):
        super().__init__(acc_num, name, open_bal, 'Deposit')
        self.interest_rate = interest_rate

    def __str__(self):
        return super().__str__() + ' interest rate is: '+ \
               str(self.interest_rate)


class InvestmentAccount(Account):
    def __init__(self, acc_num, name, open_bal, investment_type):
        super().__init__(acc_num, name, open_bal, 'Investment')
        self.investment_type = investment_type

    def __str__(self):
        return super().__str__() + ' investment type is: '+ \
               self.investment_type

