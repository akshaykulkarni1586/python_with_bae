from setuptools import setup

setup(
    name='vsearch',
    version='1.0',
    description='The Head First Python search tools',
    author='Shreya',
    author_email='karmakarshreya09@gmail.com',
    url='headfirstlabs.com',
    py_modules=['vsearch'],
)
