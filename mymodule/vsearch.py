def search4vowels(phrase: str) ->set:
    """Returns the vowels present in supplied phrase"""
    vowels = set('aeiou')
    return vowels.intersection(set(phrase))


def search4letters(phrase: str, letters: str='aeiou') ->set:
    """Returns the vowels present in supplied phrase"""
    return set(letters).intersection(set(phrase))
